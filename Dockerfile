FROM alpine:3.16

ARG LIBOWFAT_VERSION="0.32"
ARG GATLING_VERSION="0.16"
RUN apk update && \
    apk upgrade && \
    apk add openssl zlib libcap libowfat bash && \
    apk add --virtual .gatling-deps \
        gcc \
        make \
        cvs \
        libc-dev \
        openssl-dev \
        zlib-dev \
        libcap-dev \
        libowfat-dev \
        wget && \
    cd /tmp && \
    wget http://www.fefe.de/gatling/gatling-$GATLING_VERSION.tar.xz && \
    tar xf gatling-$GATLING_VERSION.tar.xz && \
    cd gatling-$GATLING_VERSION && \
    make gatling && \
    make install && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd && \
    apk del .gatling-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

USER 1001

WORKDIR /data

EXPOSE 5151

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]
